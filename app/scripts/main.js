
//data for portolio and its sections
var data = {
  projects: [
    { title: 'foo', desc: 'this is the description caption.', img: ''},
    { title: 'bar', desc: 'this is the description caption.', img: ''},
    { title: 'some', desc: 'this is the description caption.', img: ''},
    { title: 'who', desc: 'this is the description caption.', img: ''}
  ],
  designs: [
    {title: 'design number 1', desc: 'this is my desc', img:''},
    {title: 'disign number 2', desc: 'this is my desc', img: ''},
    {title: 'design number 3', desc: 'this is my desc', img: ''}
  ],
  contact: { number: '912.332.0037', email: 'wisewes@gmail.com' }
};


//component for projects
Vue.component('projects', {
  props: ['projects']
});

//component for designs
Vue.component('designs', {
  prop: ['designs']
});


/**
 * Main vue instance
 */
new Vue({
  el: 'body',
  data: {
    projects: data.projects,
    designs: data.designs
  },

  ready: function() {

  },

  methods: {

  }
});
